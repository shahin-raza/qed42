<?php

namespace Drupal\qed_migrations\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the Cities entity edit forms.
 */
class CitiesForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    $status = $entity->save();

    if ($status === SAVED_NEW) {
      $this->messenger()->addStatus($this->t('Created the %label Cities.', [
        '%label' => $entity->label(),
      ]));
    }
    else {
      $this->messenger()->addStatus($this->t('Saved the %label Cities.', [
        '%label' => $entity->label(),
      ]));
    }

    $form_state->setRedirect('entity.cities.canonical', ['cities' => $entity->id()]);
  }
}
