<?php

namespace Drupal\qed_migrations;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Provides a list controller for the Cities entity.
 */
class CustomEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['_id'] = $this->t('_ID');
    $header['city'] = $this->t('City');
    $header['loc'] = $this->t('Location');
    $header['pop'] = $this->t('Population');
    $header['state'] = $this->t('State');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\qed_migrations\Entity\Cities $entity */
    $row['id'] = $entity->id();
    $row['_id'] = $entity->get('_id')->value;
    $row['city'] = $entity->label();
    $row['loc'] = $entity->get('loc')->value;
    $row['pop'] = $entity->get('pop')->value;
    $row['state'] = $entity->get('state')->value;
    return $row + parent::buildRow($entity);
  }
}
