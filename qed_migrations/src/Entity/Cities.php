<?php
/**
 * @file
 * Contains \Drupal\qed_migrations\Entity\Advertiser.
 */

namespace Drupal\qed_migrations\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Defines the Cities entity.
 *
 * @ContentEntityType(
 *   id = "cities",
 *   label = @Translation("Cities"),
 *   base_table = "cities",
 *   admin_permission = "administer site configuration",
 *   fieldable = TRUE, 
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "label" = "city",
 *   },
 *   handlers = {
 *     "list_builder" = "Drupal\qed_migrations\CustomEntityListBuilder",
 *     "access" = "Drupal\qed_migrations\CustomEntityAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\qed_migrations\Form\CitiesForm",
 *       "add" = "Drupal\qed_migrations\Form\CitiesForm",
 *       "edit" = "Drupal\qed_migrations\Form\CitiesForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   links = {
 *     "canonical" = "/admin/cities/{cities}",
 *     "add-form" = "/admin/cities/add",
 *     "edit-form" = "/admin/cities/{cities}/edit",
 *     "delete-form" = "/admin/cities/{cities}/delete",
 *     "collection" = "/admin/content/cities",
 *   },
 *   field_ui_base_route = "entity.cities.edit_form",
 * )
 */
class Cities extends ContentEntityBase implements ContentEntityInterface {
  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('_ID'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setRequired(TRUE)
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'weight' => -4,
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form' , FALSE)
      ->setDisplayConfigurable('view' , TRUE);

    $fields['city'] = BaseFieldDefinition::create('string')
      ->setLabel(t('City'))
      ->setRequired(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form' , FALSE)
      ->setDisplayConfigurable('view' , TRUE);

    $fields['loc'] = BaseFieldDefinition::create('geolocation')
      ->setLabel(t('Location'))
      ->setRequired(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'geolocation_map',
        'weight' => 1,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'geolocation_googlegeocoder',
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['pop'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Population'))
      ->setRequired(TRUE)
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form' , FALSE)
      ->setDisplayConfigurable('view' , TRUE);

    $fields['state'] = BaseFieldDefinition::create('string')
      ->setLabel(t('State'))
      ->setSettings([
        'max_length' => 2,
        'text_processing' => 0,
      ])
      ->setRequired(TRUE)
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form' , FALSE)
      ->setDisplayConfigurable('view' , TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'));

    return $fields;
  }
}
