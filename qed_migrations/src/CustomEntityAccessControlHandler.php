<?php

namespace Drupal\qed_migrations;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Cities entity.
 */
class CustomEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, $account) {
    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view cities entity');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit cities entity');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete cities entity');

      case 'create':
        return AccessResult::allowedIfHasPermission($account, 'create cities entity');
    }

    return AccessResult::neutral();
  }
}
