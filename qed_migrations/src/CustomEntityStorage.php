<?php

namespace Drupal\qed_migrations;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Storage handler for the Custom entity.
 */
class CustomEntityStorage extends SqlContentEntityStorage {

  /**
   * {@inheritdoc}
   */
  protected function getQueryServiceName() {
    return 'entity.query.sql';
  }
}
